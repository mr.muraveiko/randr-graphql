export const onSignal = (signal: string): void => {
  switch (signal) {
    default:
      process.exit();
  }
};
