import { WRONG_TIME_ERROR } from "@utils/errors";

export const convertDayToMilliseconds = (days: number): number =>
  days * 24 * 60 * 60 * 1000;

export const roundTime = (time: string, toRound: number): string => {
  if (
    !toRound ||
    toRound <= 0 ||
    toRound > 60 ||
    !time.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
  ) {
    throw new Error(WRONG_TIME_ERROR);
  }

  const splited = time.split(":");
  const rounded =
    Math.round((+splited[0] * 60 + +splited[1]) / toRound) * toRound;

  return (
    Math.floor(rounded / 60)
      .toString()
      .padStart(2, "0") +
    ":" +
    (rounded % 60).toString().padStart(2, "0")
  );
};

export const addSubstractMinutes = (time: string, minutes: number): string => {
  if (
    (!minutes && minutes !== 0) ||
    !time.match(/^([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$/)
  ) {
    throw new Error(WRONG_TIME_ERROR);
  }

  const D = (J: number) => (J < 10 ? "0" : "") + J;
  const piece = time.split(":");
  const mins = +piece[0] * 60 + +piece[1] + minutes;

  return D(((mins % (24 * 60)) / 60) | 0) + ":" + D(mins % 60);
};
