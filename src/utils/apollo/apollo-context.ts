import { Request, Response } from "express";
import { CacheClientCreateType } from "@cache";
import RUser from "@entity/RUser";
import Company from "@entity/Company";
import Employee from "@entity/Employee";

export const apolloContext = (context?: ContextType) => ({
  req,
  res,
}: {
  req: Request;
  res: Response;
}) => {
  return {
    req,
    res,
    rUser: req.rUser,
    company: req.company,
    employee: req.employee,
    cache: req.cache || context!.cache,
  };
};

export type ContextType = {
  req: Request;
  res: Response;
  rUser?: RUser;
  company?: Company;
  employee?: Employee;
  cache: CacheClientCreateType;
};
