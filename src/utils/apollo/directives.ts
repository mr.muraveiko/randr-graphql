import { Request, Response } from "express";
import { SchemaDirectiveVisitor } from "graphql-tools";
import { AuthenticationError } from "apollo-server";
import { defaultFieldResolver } from "graphql";
import { ContextType } from "@utils";

import RUser from "@entity/RUser";
import Company from "@entity/Company";
import Employee from "@entity/Employee";

import JWT from "@core/jwt";
import { updateRefreshToken } from "@core/auth/refreshToken";
import {
  createTokens,
  getAccessTokenFromCookies,
  getRefreshTokenFromCookies,
  setTokensToCookies,
  clearTokensFromCookies,
  ITokens,
} from "@core/auth/utils";
import { TOKEN_EXPIRED_ERROR, AUTH_ERROR } from "@utils/errors";

const validateRefreshToken = async (
  req: Request
): Promise<{
  rUser: RUser;
  tokens: ITokens;
}> => {
  const refreshToken = getRefreshTokenFromCookies(req);
  const { sub: id, prm: value } = await JWT.validate(refreshToken);

  const rUser = await RUser.findOne(id, { where: { isArchived: false } });
  if (!rUser) throw new Error(AUTH_ERROR);

  const refreshTokenValue = await updateRefreshToken(rUser, value);

  return { rUser, tokens: await createTokens(rUser, refreshTokenValue) };
};

const validateAccessToken = async (
  req: Request,
  res: Response
): Promise<RUser> => {
  try {
    const accessToken = getAccessTokenFromCookies(req);
    const { sub: id } = await JWT.validate(accessToken);

    const rUser = await RUser.findOne(id, { where: { isArchived: false } });
    if (!rUser) throw new Error(AUTH_ERROR);

    return rUser;
  } catch (error) {
    if (error.message === TOKEN_EXPIRED_ERROR) {
      const { rUser, tokens } = await validateRefreshToken(req);
      setTokensToCookies(res, tokens);

      return rUser;
    }

    throw error;
  }
};

export class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: any) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function (...args: any) {
      const { req, res } = args[2] as ContextType;

      if (!req.rUser) {
        try {
          req.rUser = await validateAccessToken(req, res);
        } catch (e) {
          clearTokensFromCookies(res);
          throw new AuthenticationError("AUTHENTICATION_ERROR");
        }
      }

      return resolve.apply(this, args);
    };
  }
}

export class HasCompanyAccessDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: any) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function (...args: any) {
      const { req } = args[2] as ContextType;

      if (!req.company) {
        const { companyId } = args[1];

        if (req.rUser && companyId) {
          const company = await Company.findOne(companyId, {
            where: { isArchived: false },
          });

          if (!company) {
            throw new Error("NO COMPANY");
          }

          const employee = await Employee.findOne({
            where: { rUser: req.rUser, company, isArchived: false },
          });

          if (!employee) {
            throw new Error("NO ACCESS");
          }

          req.company = company;
          req.employee = employee;
        }
      }

      return resolve.apply(this, args);
    };
  }
}
