import express, { Express, Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import { apolloServer } from "./gql/apollo";
import connectionCreate from "./db/connectionCreate";
import mock from "./db/mock";
import { cacheClientCreate } from "@cache";
import { config } from "@config";

export default async (): Promise<Express> => {
  await connectionCreate();
  await mock();

  const app = express();

  app.use(cookieParser(config.keys.cookie));
  app.use(
    bodyParser.json({
      limit: 81920,
    })
  );

  app.use((req: Request, res: Response, next: NextFunction) => {
    req.cache = cacheClientCreate({
      host: config.redis.domain,
      port: config.redis.port,
    });
    next();
  });

  apolloServer.applyMiddleware({
    app,
    path: `/graphql`,
  });

  return app;
};
