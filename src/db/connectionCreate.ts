import { createConnection } from "typeorm";
import { config } from "@config";

import Otp from "./entity/Otp";
import RUser from "./entity/RUser";
import RefreshToken from "./entity/RefreshToken";
import Company from "./entity/Company";
import Employee from "./entity/Employee";
import Table from "./entity/Table";
import Order from "./entity/Order";
import OrderHistory from "./entity/OrderHistory";

export default async (): Promise<void> => {
  const {
    postgres: {
      config: { domain, port, user, password, database },
    },
  } = config;

  await createConnection({
    name: "default",
    type: "postgres",
    host: domain,
    port: port,
    username: user,
    password: password,
    database: database,
    dropSchema: true,
    synchronize: true,
    logging: false,
    entities: [
      RUser,
      RefreshToken,
      Otp,
      Company,
      Employee,
      Table,
      Order,
      OrderHistory,
    ],
  });
};
