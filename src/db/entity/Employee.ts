import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from "typeorm";
import { ObjectType, Field, ID } from "type-graphql";

import RUser from "./RUser";
import Company from "./Company";
import Order from "./Order";
import OrderHistory from "./OrderHistory";

export enum Roles {
  admin = "admin",
}

export interface IEmployee {
  id: string;
  role: Roles;
  isArchived: boolean;
  orders: Order[];
  closedOrders: OrderHistory[];
  company: Company;
  rUser: RUser;
}

@ObjectType()
@Entity("employee")
export default class Employee extends BaseEntity implements IEmployee {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String)
  @Column({
    type: "enum",
    enum: Roles,
    default: Roles.admin,
  })
  role: Roles;

  @Column({
    type: "boolean",
    default: false,
  })
  isArchived: boolean;

  @Field(() => [Order])
  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @Field(() => [OrderHistory])
  @OneToMany(() => OrderHistory, (orderHistory) => orderHistory.employee)
  closedOrders: OrderHistory[];

  @Field(() => Company)
  @ManyToOne(() => Company, (company) => company.employees)
  @JoinColumn()
  company: Company;

  @Field(() => RUser)
  @ManyToOne(() => RUser, (rUser) => rUser.employers)
  @JoinColumn()
  rUser: RUser;
}
