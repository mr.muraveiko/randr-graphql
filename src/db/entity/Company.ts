import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinTable,
} from "typeorm";
import { ObjectType, Field, ID, Float, Int } from "type-graphql";
import GraphQLJSONScalarType from "graphql-type-json";

import Employee from "./Employee";
import Table from "./Table";
import Order from "./Order";
import OrderHistory from "./OrderHistory";

enum Weekdays {
  Monday = "monday",
  Tuesday = "tuesday",
  Wednesday = "wednesday",
  Thursday = "thursday",
  Friday = "friday",
  Saturday = "saturday",
  Sunday = "sunday",
}

type WeekdayParams = {
  time: string;
  isActive: boolean;
  minAge: number;
  maxDuration: number;
};

export type IWorkingHours = {
  [Weekdays.Monday]: WeekdayParams;
  [Weekdays.Tuesday]: WeekdayParams;
  [Weekdays.Wednesday]: WeekdayParams;
  [Weekdays.Thursday]: WeekdayParams;
  [Weekdays.Friday]: WeekdayParams;
  [Weekdays.Saturday]: WeekdayParams;
  [Weekdays.Sunday]: WeekdayParams;
};

export interface ICompany {
  id: string;
  name: string;
  workingHours: IWorkingHours;
  city: string;
  country: string;
  address: string;
  longitude: number;
  latitude: number;
  minDuration: number;
  defaultDuration: number;
  maxFuture: number;
  isArchived: boolean;
  employees: Employee[];
  tables: Table[];
  orders: Order[];
  closedOrders: OrderHistory[];
}

@ObjectType()
@Entity("company")
export default class Company extends BaseEntity implements ICompany {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: 64,
    unique: true,
  })
  name: string;

  @Field(() => GraphQLJSONScalarType)
  @Column("simple-json")
  workingHours: IWorkingHours;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: "32",
  })
  city: string;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: "32",
  })
  country: string;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: "64",
  })
  address: string;

  @Field(() => Float, { nullable: true })
  @Column({ type: "decimal", nullable: true })
  longitude: number;

  @Field(() => Float, { nullable: true })
  @Column({ type: "decimal", nullable: true })
  latitude: number;

  @Field(() => Int)
  @Column("int")
  minDuration: number;

  @Field(() => Int)
  @Column("int")
  defaultDuration: number;

  @Field(() => Int)
  @Column("int")
  maxFuture: number;

  @Column({
    type: "boolean",
    default: false,
  })
  isArchived: boolean;

  @Field(() => [Employee])
  @OneToMany(() => Employee, (employee) => employee.company)
  employees: Employee[];

  @Field(() => [Table])
  @OneToMany(() => Table, (table) => table.company)
  @JoinTable()
  tables: Table[];

  @Field(() => [Order])
  @OneToMany(() => Order, (order) => order.company)
  orders: Order[];

  @Field(() => [OrderHistory])
  @OneToMany(() => OrderHistory, (orderHistory) => orderHistory.company)
  closedOrders: OrderHistory[];
}
