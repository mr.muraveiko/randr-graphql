import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { ObjectType, Field, Int, ID } from "type-graphql";

import Company from "./Company";
import Table from "./Table";
import Employee from "./Employee";

export enum OrderStatus {
  Confirmed = "confirmed",
  InProcess = "in_process",
}

export interface IOrder {
  id: string;
  clientPhoneNumber?: string;
  clientName?: string;
  guests: number;
  datetime: string;
  duration: number;
  status: OrderStatus;
  employee: Employee;
  table: Table;
  tableId: string;
  company: Company;
}

@ObjectType()
@Entity("order")
export default class Order extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String, { nullable: true })
  @Column({ type: "varchar", length: 20, nullable: true })
  clientPhoneNumber?: string;

  @Field(() => String, { nullable: true })
  @Column({ type: "varchar", length: 32, nullable: true })
  clientName?: string;

  @Field(() => Int)
  @Column("int")
  guests: number;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: 20,
  })
  datetime: string;

  @Field(() => Int)
  @Column("int")
  duration: number;

  @Field(() => String)
  @Column({
    type: "enum",
    enum: OrderStatus,
  })
  status: OrderStatus;

  @ManyToOne(() => Employee, (employee) => employee.orders)
  @JoinColumn()
  employee: Employee;

  @ManyToOne(() => Table, (table) => table.orders)
  @JoinColumn()
  table: Table;

  @Field(() => ID)
  @Column()
  tableId: string;

  @ManyToOne(() => Company, (company) => company.orders)
  @JoinColumn()
  company: Company;
}
