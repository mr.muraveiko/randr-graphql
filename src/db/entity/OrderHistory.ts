import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { ObjectType, Field, Int, ID } from "type-graphql";

import Company from "./Company";
import Table from "./Table";
import Employee from "./Employee";

export enum OrderHistoryStatus {
  Canceled = "canceled",
  Successed = "successed",
}

export interface IOrderHistory {
  id: string;
  clientPhoneNumber?: string;
  clientName?: string;
  guests: number;
  datetime: string;
  duration: number;
  status: OrderHistoryStatus;
  comment?: string;
  employee: Employee;
  table: Table;
  company: Company;
}

@ObjectType()
@Entity("order_history")
export default class OrderHistory extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String, { nullable: true })
  @Column({ type: "varchar", length: 20, nullable: true })
  clientPhoneNumber?: string;

  @Field(() => String, { nullable: true })
  @Column({ type: "varchar", length: 32, nullable: true })
  clientName?: string;

  @Field(() => Int)
  @Column("int")
  guests: number;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: 20,
  })
  datetime: string;

  @Field(() => Int)
  @Column("int")
  duration: number;

  @Field(() => String)
  @Column({
    type: "enum",
    enum: OrderHistoryStatus,
  })
  status: OrderHistoryStatus;

  @Field(() => String, { nullable: true })
  @Column({
    type: "varchar",
    length: 255,
    nullable: true,
  })
  comment?: string;

  @Column({
    type: "boolean",
    default: false,
  })
  isArchived: boolean;

  @ManyToOne(() => Employee, (employee) => employee.closedOrders)
  @JoinColumn()
  employee: Employee;

  @ManyToOne(() => Table, (table) => table.closedOrders)
  @JoinColumn()
  table: Table;

  @ManyToOne(() => Company, (company) => company.closedOrders)
  @JoinColumn()
  company: Company;
}
