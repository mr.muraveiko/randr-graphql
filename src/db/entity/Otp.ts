import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from "typeorm";

export interface IOtp {
  id: string;
  phoneNumber: string;
  dates: string[];
}

@Entity("otp")
export default class Otp extends BaseEntity implements IOtp {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({
    type: "varchar",
    length: 20,
    unique: true,
  })
  phoneNumber: string;

  @Column({ type: "simple-array" })
  dates: string[];
}
