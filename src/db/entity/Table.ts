import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  JoinTable,
} from "typeorm";
import { ObjectType, Field, Int, ID } from "type-graphql";

import Company from "./Company";
import Order from "./Order";
import OrderHistory from "./OrderHistory";

export interface ITable {
  id: string;
  isActive: boolean;
  isBusy: boolean;
  No: string;
  isArchived: boolean;
  company: Company;
  orders: Order[];
  closedOrders: OrderHistory[];
}

@ObjectType()
@Entity("table")
export default class Table extends BaseEntity implements ITable {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => Boolean)
  @Column({ type: "boolean", default: true })
  isActive: boolean;

  @Field(() => Boolean)
  @Column({ type: "boolean", default: false })
  isBusy: boolean;

  @Field(() => String)
  @Column({ type: "varchar", length: "2" })
  No: string;

  @Field(() => Int)
  @Column("int")
  guests: number;

  @Column({
    type: "boolean",
    default: false,
  })
  isArchived: boolean;

  @ManyToOne(() => Company, (company) => company.tables)
  company: Company;

  @OneToMany(() => Order, (order) => order.table)
  @JoinTable()
  orders: Order[];

  @OneToMany(() => OrderHistory, (orderHistory) => orderHistory.table)
  @JoinTable()
  closedOrders: OrderHistory[];
}
