import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from "typeorm";
import { ObjectType, Field, ID } from "type-graphql";

import RefreshToken from "./RefreshToken";
import Employee from "./Employee";

export interface IRUser {
  id: string;
  phoneNumber: string;
  email?: string;
  username?: string;
  ip?: string;
  lastAuth?: string;
  isArchived: boolean;
  refreshTokens: RefreshToken[];
  employers: Employee[];
}

@ObjectType()
@Entity("rUser")
export default class RUser extends BaseEntity implements IRUser {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Field(() => String)
  @Column({
    type: "varchar",
    length: 20,
    unique: true,
  })
  phoneNumber: string;

  @Field(() => String, { nullable: true })
  @Column({
    type: "varchar",
    length: 32,
    default: "RandR Пользователь",
    nullable: true,
  })
  username: string;

  @Field(() => String, { nullable: true })
  @Column({
    type: "varchar",
    length: 64,
    nullable: true,
    unique: true,
  })
  email: string;

  @Column({
    type: "varchar",
    length: 15,
    nullable: true,
  })
  ip: string;

  @Column({
    type: "varchar",
    length: 20,
    nullable: true,
  })
  lastAuth: string;

  @Column({
    type: "boolean",
    default: false,
  })
  isArchived: boolean;

  @OneToMany(() => RefreshToken, (refreshToken) => refreshToken.rUser)
  refreshTokens: RefreshToken[];

  @Field(() => [Employee])
  @OneToMany(() => Employee, (employer) => employer.rUser)
  employers: Employee[];
}
