import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";

import RUser from "./RUser";

export interface IRefreshToken {
  id: string;
  value: string;
  date: string;
  rUser: RUser;
  rUserId: string;
}

@Entity("refresh_token")
export default class RefreshToken extends BaseEntity implements IRefreshToken {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({
    type: "varchar",
    length: 43,
  })
  value: string;

  @Column({
    type: "varchar",
    length: 20,
  })
  date: string;

  @ManyToOne(() => RUser, (rUser) => rUser.refreshTokens)
  @JoinColumn()
  rUser: RUser;

  @Column()
  rUserId: string;
}
