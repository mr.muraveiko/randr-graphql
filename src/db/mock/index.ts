import RUser from "@entity/RUser";
import Company from "@entity/Company";
import Employee from "@entity/Employee";
import Table from "@entity/Table";

export default async () => {
  const rUser = new RUser();
  rUser.phoneNumber = "+375 (29) 373-55-17";
  rUser.id = "4583fc30-5a27-48f0-915c-8e9b3fb2f8aa";
  rUser.username = "Denis";
  rUser.email = "mr.muraveiko@gmail.com";
  await rUser.save();

  const company = new Company();
  company.id = "2f161a34-ad10-43bf-8049-c89642df184f";
  company.name = "RandR Spot";
  company.workingHours = {
    monday: {
      time: "09:00-23:00",
      isActive: true,
      minAge: 12,
      maxDuration: 240,
    },
    tuesday: {
      time: "09:00-23:00",
      isActive: true,
      minAge: 12,
      maxDuration: 240,
    },
    wednesday: {
      time: "09:00-23:00",
      isActive: true,
      minAge: 12,
      maxDuration: 240,
    },
    thursday: {
      time: "09:00-23:00",
      isActive: false,
      minAge: 12,
      maxDuration: 240,
    },
    friday: {
      time: "09:00-02:00",
      isActive: true,
      minAge: 18,
      maxDuration: 180,
    },
    saturday: {
      time: "09:00-02:00",
      isActive: false,
      minAge: 18,
      maxDuration: 180,
    },
    sunday: {
      time: "09:00-02:00",
      isActive: true,
      minAge: 18,
      maxDuration: 180,
    },
  };

  company.minDuration = 45;
  company.defaultDuration = 120;
  company.maxFuture = 14;

  company.city = "minsk";
  company.country = "belarus";
  company.address = "пр-т. Победителей, 89/3";

  await company.save();

  const table1 = new Table();
  table1.No = "1";
  table1.guests = 2;
  await table1.save();

  const table2 = new Table();
  table2.id = "e16ecf56-75dc-486f-8122-ea571847ab76";
  table2.No = "2";
  table2.guests = 4;
  await table2.save();

  company.tables = [table1, table2];

  const employee = new Employee();
  employee.rUser = rUser;
  employee.company = company;
  await employee.save();

  await company.save();
};
