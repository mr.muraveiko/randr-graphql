import { createClient, RedisClient } from "redis";
import { config } from "@config";
import { reject } from "lodash";

const cacheInit = ({
  host,
  port,
}: {
  host?: string;
  port?: number;
}): RedisClient =>
  createClient({
    host: host || config.redis.domain,
    port: port || config.redis.port,
  });

type ValueType =
  | string
  | number
  | {
      [key: string]: string | number;
    }
  | (string | number)[];

export const cacheClientCreate = (credentials: {
  port?: number;
  host?: string;
}) => {
  const cacheManager = cacheInit(credentials);

  return {
    /***
     * value должно быть объектом имей в виду
     *
     * @example
     * req.cache.save({ key: '1', value: { test: Date.now() } });
     */
    save: ({ key, value }: { key: string; value: ValueType }) => {
      return new Promise<boolean>((resolve, reject) => {
        cacheManager.hmset(key, value, (err: Error | null) => {
          if (err) {
            reject(err);
          } else {
            resolve(true);
          }
        });
      });
    },
    saveTemporary: ({
      key,
      value,
      time,
    }: {
      key: string;
      value: ValueType;
      time: number;
    }) => {
      return new Promise<boolean>((resolve, reject) => {
        cacheManager.hmset(key, value, (err: Error | null) => {
          if (err) {
            reject(err);
          } else {
            cacheManager.expire(key, time);
            resolve(true);
          }
        });
      });
    },
    get: <T>(key: string) => {
      return new Promise<
        | T
        | null
        | {
            [key: string]: string;
          }
      >((resolve, reject) => {
        cacheManager.hgetall(key, (err: Error | null, reply: any) => {
          if (err) {
            reject(err);
          }
          resolve(reply);
        });
      });
    },
    getExpirationTime: (key: string) => {
      return new Promise<number>((resolve) => {
        cacheManager.ttl(key, (err: Error | null, data: number) => {
          if (err) {
            reject(err);
          }
          resolve(data);
        });
      });
    },
    deleteKey: (key: string) => {
      return new Promise<boolean>((resolve, reject) => {
        cacheManager.del(key, (err: Error | null, response: any) => {
          if (err) {
            reject(err);
          }
          resolve(response === 1);
        });
      });
    },
  };
};

export type CacheClientCreateType = ReturnType<typeof cacheClientCreate>;
