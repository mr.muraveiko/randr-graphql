import "module-alias/register";
import "reflect-metadata";
import http from "http";
import { config } from "@config";
import { onSignal } from "@utils";
import appCreate from "../app";

appCreate()
  .then((app) => {
    const port = config.port;
    app.set("port", port);

    const server = http.createServer(app);
    server.listen(port);

    server.on("listening", () => {
      const addr = server.address();
      const bind = typeof addr === "string" ? `Pipe ${port}` : `Port ${port}`;
      console.log(`Listening on ${bind}`);
    });

    server.on("error", (error: any) => {
      console.error(error);
      if (error.syscall !== "listen") {
        throw error;
      }
      switch (error.code) {
        default:
          throw error;
      }
    });

    process.on("SIGINT", onSignal);
    process.on("SIGTERM", onSignal);
    process.on("SIGQUIT", onSignal);
  })
  .catch((err) => console.log(err));
