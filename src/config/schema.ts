import Joi from "joi";

export const schema = Joi.object()
  .keys({
    NODE_ENV: Joi.string()
      .allow(["development", "production"])
      .default("development"),

    PORT: Joi.number().required(),

    ORDER_DELTA: Joi.number().required(),

    DATABASE_POSTGRES_DOMAIN: Joi.string().required(),
    DATABASE_POSTGRES_USER: Joi.string().required(),
    DATABASE_POSTGRES_DATABASE: Joi.string().required(),
    DATABASE_POSTGRES_PASSWORD: Joi.string().required(),
    DATABASE_POSTGRES_PORT: Joi.number().required(),

    DATABASE_PGADMIN_DEFAULT_EMAIL: Joi.string().required(),
    DATABASE_PGADMIN_DEFAULT_PASSWORD: Joi.string().required(),
    DATABASE_PGADMIN_PORT: Joi.number().required(),

    REDIS_DOMAIN: Joi.string().required(),
    REDIS_PORT: Joi.number().required(),

    COOKIE_KEY: Joi.string().required(),

    TOKEN_ACCESS_VALIDITY_DAYS: Joi.number().required(),
    TOKEN_REFRESH_VALIDITY_DAYS: Joi.number().required(),
    TOKEN_ISSUER: Joi.string().required(),
    TOKEN_AUDIENCE: Joi.string().required(),
  })
  .unknown()
  .required();
