import Joi from "joi";
import path from "path";
import fs from "fs";
import { parse } from "dotenv";
import { schema } from "./schema";

const { error, value } = Joi.validate(
  parse(fs.readFileSync(path.resolve(process.cwd(), `.env`))),
  schema
);

if (error) {
  throw new Error(`Config validation: ${error.message}`);
}

const config = {
  env: value.NODE_ENV,
  port: Number(value.PORT),
  orderDelta: Number(value.ORDER_DELTA),
  postgres: {
    config: {
      domain: value.DATABASE_POSTGRES_DOMAIN,
      user: value.DATABASE_POSTGRES_USER,
      database: value.DATABASE_POSTGRES_DATABASE,
      password: value.DATABASE_POSTGRES_PASSWORD,
      port: Number(value.DATABASE_POSTGRES_PORT),
    },
    pgadmin: {
      defaultEmail: value.DATABASE_PGADMIN_DEFAULT_EMAIL,
      defaultPassword: value.DATABASE_PGADMIN_DEFAULT_PASSWORD,
      port: Number(value.DATABASE_PGADMIN_PORT),
    },
  },
  redis: {
    domain: value.REDIS_DOMAIN,
    port: Number(value.REDIS_PORT),
  },
  keys: {
    cookie: value.COOKIE_KEY,
  },
  token: {
    accessTokenValidityDays: Number(value.TOKEN_ACCESS_VALIDITY_DAYS),
    refreshTokenValidityDays: Number(value.TOKEN_REFRESH_VALIDITY_DAYS),
    issuer: value.TOKEN_ISSUER,
    audience: value.TOKEN_AUDIENCE,
  },
};

export { config };
