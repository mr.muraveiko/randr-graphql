import moment, { Moment } from "moment";
import _ from "lodash";

import { config } from "@config";
const orderDelta = config.orderDelta;

import Company, { IWorkingHours } from "@entity/Company";
import Order from "@entity/Order";
import Table from "@entity/Table";

import { roundTime, addSubstractMinutes } from "@utils";
import { WRONG_TIME_ERROR, WRONG_DATE_ERROR } from "@utils/errors";

export type ITimetable = {
  [index: string]: Array<string>;
};

export default class Timetable {
  private _orders: Array<Order>;
  private _tables: Array<Table>;
  private _workingHours: IWorkingHours;
  private _defaultDuration: number;
  private _minDuration: number;
  private _maxFuture: number;

  constructor(company: Company, tables: Array<Table>, orders: Array<Order>) {
    this._tables = tables;
    this._orders = orders;
    this._workingHours = company.workingHours;
    this._defaultDuration = company.defaultDuration;
    this._minDuration = company.minDuration;
    this._maxFuture = company.maxFuture;
  }

  private copyOrders = (ignoreIds: Array<string> = []): Array<Order> => {
    const orders = _.clone(this._orders);
    return _.isEmpty(ignoreIds)
      ? orders
      : orders.filter((order) => !ignoreIds.includes(order.id));
  };

  getTimetable(datetime?: Moment, ignoreIds?: Array<string>): ITimetable {
    const { orders, start, end } = this.calculateTime(
      this.copyOrders(ignoreIds),
      datetime
    );

    const t: Array<string> = [];
    while (start <= end) {
      t.push(start.format("HH:mm"));
      start.add(orderDelta, "minutes");
    }

    return this._tables.reduce((acc, table) => {
      const tableOrders = orders.filter((order) => order.tableId === table.id);
      const key = `${table.id}:${table.guests}`;
      const values = _.clone(t);

      tableOrders.forEach((order) => {
        const datetime = moment.utc(order.datetime).format("HH:mm");
        for (let i = 0; i < this._minDuration / orderDelta; i++) {
          _.pull(values, addSubstractMinutes(datetime, -orderDelta * i));
        }
        for (let i = 0; i < order.duration / orderDelta; i++) {
          _.pull(values, addSubstractMinutes(datetime, orderDelta * i));
        }
      });
      // @ts-ignore
      acc[key] = values;
      return acc;
    }, {});
  }

  getAvailableTables(
    timetable: ITimetable,
    time = roundTime(moment.utc().format("HH:mm"), orderDelta),
    duration = this._defaultDuration,
    guests = 1
  ): Array<string> {
    if (
      !time.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) ||
      +time.split(":")[1] % orderDelta !== 0
    ) {
      throw new Error(WRONG_TIME_ERROR);
    }

    if (duration < this._minDuration || duration % orderDelta !== 0) {
      throw new Error("WRONG_DURATION");
    }

    const t: Array<string> = [];
    const endTime = addSubstractMinutes(time, duration - this._minDuration);
    while (time != endTime) {
      t.push(time);
      time = addSubstractMinutes(time, orderDelta);
    }
    t.push(time);

    const tables: Array<string> = [];
    for (const time in timetable) {
      if (
        guests <= Number(time.split(":")[1]) &&
        _.isEmpty(_.difference(t, timetable[time]))
      ) {
        tables.push(time);
      }
    }

    return tables;
  }

  clearTimetable = (timetable: ITimetable): ITimetable => {
    const companyTimetable: ITimetable = {};

    for (const key in timetable) {
      const guests = key.split(":")[1];
      if (!companyTimetable[guests]) companyTimetable[guests] = [];
    }

    for (const time in companyTimetable) {
      const before: Array<string> = [];
      const after: Array<string> = [];
      for (const free in timetable) {
        if (free.split(":")[1] >= time) {
          const startTime = timetable[free][0];

          timetable[free].forEach((o) => {
            if (o < startTime) before.push(o);
            else after.push(o);
          });
        }
      }
      companyTimetable[time] = [
        ...new Set(after.sort()),
        ...new Set(before.sort()),
      ];
    }

    return companyTimetable;
  };

  static suggestTable = (
    timetable: ITimetable,
    tables: Array<string>
  ): number => {
    const avTables: Array<{
      id: string;
      guests: number;
      length: number;
    }> = [];
    for (const time in timetable) {
      if (tables.includes(time)) {
        const id = time.split(":");
        avTables.push({
          id: id[0],
          guests: Number(id[1]),
          length: timetable[time].length,
        });
      }
    }
    return Number(
      avTables.sort((a, b) => a.guests - b.guests || a.length - b.length)[0].id
    );
  };

  private getCheckedDate = (date?: Moment): Moment => {
    if (!date) return moment.utc();

    const formated = date.format("YYYY-MM-DD");
    if (
      formated < moment.utc().format("YYYY-MM-DD") ||
      formated > moment.utc().add(this._maxFuture, "day").format("YYYY-MM-DD")
    )
      throw new Error(WRONG_DATE_ERROR);
    return date;
  };

  private calculateTime = (
    orders: Array<Order>,
    date?: Moment
  ): { orders: Array<Order>; start: Moment; end: Moment } => {
    const datetime = this.getCheckedDate(date);
    // @ts-ignore
    const dayWorkingHours = this._workingHours[
      datetime.format("dddd").toLowerCase()
    ];
    const splitted = dayWorkingHours.time.split("-");

    const formated = datetime.format("YYYY-MM-DD");
    const end = moment.utc(`${formated}T${splitted[1]}`);
    if (splitted[0] >= splitted[1]) end.add(1, "day");
    end.subtract(this._minDuration, "minutes");

    const now = moment.utc();
    const startTime = moment.utc(`${formated}T${splitted[0]}`);
    const start =
      now >= startTime
        ? moment.utc(
            `${formated}T${roundTime(now.format("HH:mm"), orderDelta)}`
          )
        : startTime;

    return {
      orders: orders.filter(
        (order) =>
          moment.utc(order.datetime) >=
            start.clone().subtract(dayWorkingHours.maxDuration, "minutes") &&
          moment.utc(order.datetime) <= end
      ),
      start,
      end,
    };
  };

  static async build(company: Company): Promise<Timetable> {
    const tables = await Table.find({ where: { company } });
    const orders = await Order.find({ where: { company } });

    return new Timetable(company, tables, orders);
  }
}
