declare namespace Express {
  interface Request {
    cache: import("@cache").CacheClientCreateType;
    rUser?: import("@entity/RUser").default;
    company?: import("@entity/Company").default;
    employee?: import("@entity/Employee").default;
  }
}
