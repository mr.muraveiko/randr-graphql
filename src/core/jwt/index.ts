import path from "path";
import { sign, verify } from "jsonwebtoken";
import { promisify } from "util";
import { readFile } from "fs";

import { config } from "@config";
import {
  TOKEN_EXPIRED_ERROR,
  TOKEN_GENERATION_FAILURE_ERROR,
  BAD_TOKEN_ERROR,
  INVALID_ACCESS_TOKEN_ERROR,
} from "@utils/errors";

export default class JWT {
  private static async readPublicKey(): Promise<string> {
    return promisify(readFile)(
      path.join(__dirname, "./keys/public.pem"),
      "utf8"
    );
  }

  private static async readPrivateKey(): Promise<string> {
    return promisify(readFile)(
      path.join(__dirname, "./keys/private.pem"),
      "utf8"
    );
  }

  private static validateTokenData = (payload: JwtPayload): boolean => {
    if (
      !payload ||
      !payload.iss ||
      !payload.sub ||
      !payload.aud ||
      !payload.prm ||
      payload.iss !== config.token.issuer ||
      payload.aud !== config.token.audience
    )
      throw new Error(INVALID_ACCESS_TOKEN_ERROR);
    return true;
  };

  public static async encode(payload: JwtPayload): Promise<string> {
    const cert = await this.readPrivateKey();
    if (!cert) throw new Error(TOKEN_GENERATION_FAILURE_ERROR);
    // @ts-ignore
    return promisify(sign)({ ...payload }, cert, { algorithm: "RS256" });
  }

  public static async validate(token: string): Promise<JwtPayload> {
    const cert = await this.readPublicKey();
    try {
      // @ts-ignore
      const payload: JwtPayload = await promisify(verify)(token, cert);
      this.validateTokenData(payload);

      return payload;
    } catch (e) {
      if (e && e.name === "TokenExpiredError")
        throw new Error(TOKEN_EXPIRED_ERROR);
      throw new Error(BAD_TOKEN_ERROR);
    }
  }

  public static async decode(token: string): Promise<JwtPayload> {
    const cert = await this.readPublicKey();
    try {
      // @ts-ignore
      const payload: JwtPayload = await promisify(verify)(token, cert, {
        ignoreExpiration: true,
      });
      this.validateTokenData(payload);

      return payload;
    } catch (e) {
      throw new Error(TOKEN_EXPIRED_ERROR);
    }
  }
}

export class JwtPayload {
  aud: string;
  sub: string;
  iss: string;
  iat: number;
  exp: number;
  prm: string;

  constructor(
    issuer: string,
    audience: string,
    subject: string,
    param: string,
    validity: number
  ) {
    this.iss = issuer;
    this.aud = audience;
    this.sub = subject;
    this.iat = Math.floor(Date.now() / 1000);
    this.exp = this.iat + validity * 24 * 60 * 60;
    this.prm = param;
  }
}
