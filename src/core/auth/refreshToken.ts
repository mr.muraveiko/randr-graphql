import moment from "moment";

import RUser from "@entity/RUser";
import RefreshToken from "@entity/RefreshToken";

import { generateToken } from "@utils";
import { TOKEN_EXPIRED_ERROR, INVALID_ACCESS_TOKEN_ERROR } from "@utils/errors";

export const createRefreshToken = async (rUser: RUser): Promise<string> => {
  const [refreshTokens, length] = await RefreshToken.findAndCount({
    where: { rUser },
  });

  if (length >= 10) {
    await Promise.all(
      refreshTokens.map(async (refreshToken) => {
        await refreshToken.remove();
      })
    );
  }

  const refreshToken = new RefreshToken();

  refreshToken.rUser = rUser;
  refreshToken.date = moment.utc().format();
  refreshToken.value = generateToken();

  await refreshToken.save();

  return refreshToken.value;
};

export const updateRefreshToken = async (
  rUser: RUser,
  value: string
): Promise<string> => {
  const refreshToken = await RefreshToken.findOne({ where: { rUser, value } });
  if (!refreshToken) {
    throw new Error(INVALID_ACCESS_TOKEN_ERROR);
  }

  if (
    moment
      .duration(moment.utc().diff(moment.utc(refreshToken.date)))
      .asMonths() > 1
  ) {
    await refreshToken.remove();
    throw new Error(TOKEN_EXPIRED_ERROR);
  }

  refreshToken.value = generateToken();
  refreshToken.date = moment.utc().format();

  await refreshToken.save();

  return refreshToken.value;
};
