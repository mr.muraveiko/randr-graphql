import { Request, Response } from "express";

import { config } from "@config";
import { convertDayToMilliseconds } from "@utils";
import { BAD_TOKEN_ERROR } from "@utils/errors";
import JWT, { JwtPayload } from "@core/jwt";
import RUser from "@entity/RUser";

export declare interface ITokens {
  accessToken: string;
  refreshToken: string;
}

const cookieTokenKeys = {
  access: "__access-token",
  refresh: "__refresh-token",
};

export const createTokens = async (
  rUser: RUser,
  refreshTokenValue: string
): Promise<ITokens> => {
  const issuer = config.token.issuer;
  const audience = config.token.audience;
  const accessTokenValidityDays = config.token.accessTokenValidityDays;
  const refreshTokenValidityDays = config.token.refreshTokenValidityDays;

  const accessToken = await JWT.encode(
    new JwtPayload(
      issuer,
      audience,
      rUser.id,
      rUser.phoneNumber,
      accessTokenValidityDays
    )
  );
  if (!accessToken) throw new Error(BAD_TOKEN_ERROR);

  const refreshToken = await JWT.encode(
    new JwtPayload(
      issuer,
      audience,
      rUser.id,
      refreshTokenValue,
      refreshTokenValidityDays
    )
  );
  if (!refreshToken) throw new Error(BAD_TOKEN_ERROR);

  return { accessToken, refreshToken } as ITokens;
};

export const setTokensToCookies = (res: Response, tokens: ITokens): void => {
  res.cookie(cookieTokenKeys.access, tokens.accessToken, {
    maxAge: convertDayToMilliseconds(config.token.accessTokenValidityDays),
    httpOnly: true,
    signed: true,
  });

  res.cookie(cookieTokenKeys.refresh, tokens.refreshToken, {
    maxAge: convertDayToMilliseconds(config.token.refreshTokenValidityDays),
    httpOnly: true,
    signed: true,
  });
};

export const getTokensFromCookies = (req: Request): ITokens => {
  const accessToken = req.signedCookies[cookieTokenKeys.access];
  const refreshToken = req.signedCookies[cookieTokenKeys.refresh];
  return { accessToken, refreshToken } as ITokens;
};

export const getAccessTokenFromCookies = (req: Request): string => {
  return req.signedCookies[cookieTokenKeys.access];
};

export const getRefreshTokenFromCookies = (req: Request): string => {
  return req.signedCookies[cookieTokenKeys.refresh];
};

export const clearTokensFromCookies = (res: Response): void => {
  res.clearCookie(cookieTokenKeys.access);
  res.clearCookie(cookieTokenKeys.refresh);
};
