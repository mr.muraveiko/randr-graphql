import moment from "moment";

import RUser from "@entity/RUser";

import { createRefreshToken } from "./refreshToken";
import { createTokens, ITokens } from "./utils";

export const authByPhoneNumber = async (
  phoneNumber: string,
  ip: string | null
): Promise<{ rUser: RUser; tokens: ITokens }> => {
  const rUser = await RUser.findOne({
    where: { phoneNumber, isArchived: false },
  }).then((data) => {
    if (!data) {
      const newUser = new RUser();
      newUser.phoneNumber = phoneNumber;

      return newUser;
    }

    return data;
  });

  rUser.lastAuth = moment.utc().format();
  if (ip) {
    rUser.ip = ip;
  }

  await rUser.save();

  const refreshTokenValue = await createRefreshToken(rUser);
  const tokens = await createTokens(rUser, refreshTokenValue);

  return { rUser, tokens };
};
