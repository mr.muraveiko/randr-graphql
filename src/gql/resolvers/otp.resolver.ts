import {
  Resolver,
  Mutation,
  Field,
  Args,
  ArgsType,
  Int,
  Ctx,
} from "type-graphql";
import { ApolloError } from "apollo-server";
import { IsPhoneNumber, Min, Max } from "class-validator";
import moment from "moment";
import { getClientIp } from "request-ip";

import RUser from "@entity/RUser";
import Otp from "@entity/Otp";

import { authByPhoneNumber } from "@core/auth/controller";
import { setTokensToCookies } from "@core/auth/utils";
import { ContextType } from "@utils";

import {
  TRY_AGAIN_LATER_ERROR,
  WRONG_OTP_ERROR,
  REQUEST_NEW_OTP_ERROR,
} from "@utils/errors";

@ArgsType()
class RequestOtpArg {
  @Field()
  @IsPhoneNumber("BY")
  phoneNumber: string;
}

@ArgsType()
class ConfirmOtpArgs {
  @Field()
  @IsPhoneNumber("BY")
  phoneNumber: string;

  @Field(() => Int)
  @Min(1000)
  @Max(9999)
  value: number;
}

@Resolver()
export default class OtpResolver {
  @Mutation(() => Boolean)
  async requestOtp(
    @Args() { phoneNumber }: RequestOtpArg,
    @Ctx() { cache }: ContextType
  ): Promise<boolean> {
    const otp = await Otp.findOne({ phoneNumber }).then((data) => {
      if (!data) {
        const newOtp = new Otp();
        newOtp.phoneNumber = phoneNumber;
        newOtp.dates = [];
        return newOtp;
      }
      return data;
    });

    if (otp.dates.length) {
      const requestedThisDay = otp.dates.filter(
        (date) => date >= moment.utc().subtract(1, "day").format()
      ).length;
      switch (requestedThisDay) {
        case 1: {
          if (
            moment
              .duration(moment.utc().diff(moment.utc(otp.dates[0])))
              .asMinutes() < 5
          ) {
            throw new Error(TRY_AGAIN_LATER_ERROR);
          }

          break;
        }
        case 2: {
          if (
            moment
              .duration(moment.utc().diff(moment.utc(otp.dates[1])))
              .asHours() < 1
          ) {
            throw new Error(TRY_AGAIN_LATER_ERROR);
          }

          break;
        }
        default: {
          throw new Error(TRY_AGAIN_LATER_ERROR);
        }
      }
    }

    if (otp.dates.length >= 3) {
      otp.dates.shift();
    }
    otp.dates.push(moment.utc().format());

    await otp.save();

    const value = Math.floor(1000 + Math.random() * 9000);
    const tokenSaved = await cache.saveTemporary({
      key: phoneNumber,
      value: {
        attempts: 3,
        value,
      },
      time: 310,
    });

    if (!tokenSaved) {
      throw new Error("Smth went wrong");
    }

    console.log({ [phoneNumber]: value });

    return true;
  }

  @Mutation(() => RUser)
  async confirmOtp(
    @Args() { phoneNumber, value }: ConfirmOtpArgs,
    @Ctx() { req, res, cache }: ContextType
  ): Promise<RUser> {
    try {
      const token = await cache.get<{ value: string; attempts: string }>(
        phoneNumber
      );

      if (!token) {
        throw new Error("NO_TOKEN");
      }

      if (+token.value !== value) {
        const attempts = +token.attempts;
        if (attempts <= 1) {
          await cache.deleteKey(phoneNumber);
          throw new Error("NO_ATTEMPTS");
        }

        const expTime = await cache.getExpirationTime(phoneNumber);
        if (!expTime || expTime < 3) {
          throw new Error("NO_TIME");
        }

        await cache.saveTemporary({
          key: phoneNumber,
          value: {
            value: token.value,
            attempts: attempts - 1,
          },
          time: expTime,
        });

        throw new Error(WRONG_OTP_ERROR);
      }

      await cache.deleteKey(phoneNumber);

      const { rUser, tokens } = await authByPhoneNumber(
        phoneNumber,
        getClientIp(req)
      );

      setTokensToCookies(res, tokens);

      return rUser;
    } catch (e) {
      console.log(e);
      throw new ApolloError(
        e.message === WRONG_OTP_ERROR ? WRONG_OTP_ERROR : REQUEST_NEW_OTP_ERROR,
        "400"
      );
    }
  }
}
