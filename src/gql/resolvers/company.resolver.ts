import {
  Resolver,
  FieldResolver,
  Root,
  Mutation,
  Query,
  ArgsType,
  Args,
  Arg,
  Ctx,
  Field,
  Directive,
} from "type-graphql";
import { IsPhoneNumber, Matches } from "class-validator";
import { ApolloError } from "apollo-server";
import GraphQLJSONScalarType from "graphql-type-json";
import moment from "moment";

import RUser from "@entity/RUser";
import Company from "@entity/Company";
import Order from "@entity/Order";
import Table from "@entity/Table";
import Employee from "@entity/Employee";
import OrderHistory from "@entity/OrderHistory";

import TimetableService from "@services/Timetable";
import { ContextType } from "@utils";

import { WRONG_PHONE_NUMBER_ERROR } from "@utils/errors";

@ArgsType()
class timetableArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String, { nullable: true })
  @Matches(/^(\d{4})-(\d{1,2})-(\d{1,2})$/)
  date?: string;
}

@ArgsType()
class memberArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String)
  @IsPhoneNumber("BY")
  phoneNumber: string;
}

@Resolver(() => Company)
export default class CompanyResolver {
  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Query(() => Company)
  async company(
    @Arg("companyId") companyId: string,
    @Ctx() { req: { company } }: ContextType
  ): Promise<Company> {
    return company!;
  }

  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Query(() => GraphQLJSONScalarType)
  async timetable(
    @Args() { date }: timetableArgs,
    @Ctx() { req: { company } }: ContextType
  ): Promise<{ [index: string]: string[] }> {
    const timetableService = await TimetableService.build(company!);

    return timetableService.clearTimetable(
      timetableService.getTimetable(moment.utc(date))
    );
  }

  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Boolean)
  async addEmployee(
    @Args() { phoneNumber }: memberArgs,
    @Ctx() { req: { company } }: ContextType
  ): Promise<Employee> {
    try {
      const rUser = await RUser.findOne({ where: { phoneNumber } });

      if (!rUser) {
        throw new Error(WRONG_PHONE_NUMBER_ERROR);
      }

      const employeeToAdd = await Employee.findOne({
        where: { rUser, company },
      });

      if (employeeToAdd) {
        throw new Error("ALREADY ADDED");
      }

      const employee = new Employee();
      employee.company = company!;
      employee.rUser = rUser;
      await employee.save();

      return employee;
    } catch (e) {
      console.log(e);
      throw new ApolloError("SMTH BAD", "400");
    }
  }

  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Boolean)
  async removeEmployee(
    @Args() { phoneNumber }: memberArgs,
    @Ctx() { req: { company, employee } }: ContextType
  ): Promise<boolean> {
    try {
      const rUser = await RUser.findOne({ where: { phoneNumber } });

      if (!rUser) {
        throw new Error(WRONG_PHONE_NUMBER_ERROR);
      }

      const employeeToRemove = await Employee.findOne({
        where: {
          company,
          rUser,
        },
      });

      if (!employeeToRemove || employeeToRemove.id === employee?.id) {
        throw new Error(WRONG_PHONE_NUMBER_ERROR);
      }

      await employeeToRemove.remove();

      return true;
    } catch (e) {
      console.log(e);
      throw new ApolloError("SMTH BAD", "400");
    }
  }

  @FieldResolver()
  async orders(@Root() company: Company): Promise<Array<Order>> {
    const orders = await Order.find({
      where: { company },
    });

    return orders;
  }

  @FieldResolver()
  async closedOrders(@Root() company: Company): Promise<Array<OrderHistory>> {
    const orders = await OrderHistory.find({
      where: { company },
    });

    return orders;
  }

  @FieldResolver()
  async tables(@Root() company: Company): Promise<Array<Table>> {
    const tables = await Table.find({
      where: { company, isArchived: false },
    });

    return tables;
  }

  @FieldResolver()
  async employees(@Root() company: Company): Promise<Array<Employee>> {
    const employees = await Employee.find({
      where: { company, isArchived: false },
      relations: ["rUser"],
    });

    return employees;
  }
}
