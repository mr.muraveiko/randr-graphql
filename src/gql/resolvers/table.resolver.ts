import {
  Resolver,
  Mutation,
  ArgsType,
  Args,
  Ctx,
  Field,
  Directive,
  Int,
} from "type-graphql";
import { ApolloError } from "apollo-server";
import { getManager } from "typeorm";
import moment from "moment";

import TimetableService from "@services/Timetable";
import OrderHistory, { OrderHistoryStatus } from "@entity/OrderHistory";
import Order, { OrderStatus } from "@entity/Order";
import Table from "@entity/Table";

import { ContextType, roundTime } from "@utils";
import { config } from "@config";

@ArgsType()
class openTableArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String)
  orderId: string;

  @Field(() => String, { nullable: true })
  tableId?: string;

  @Field(() => Int, { nullable: true })
  duration?: number;

  @Field(() => Int, { nullable: true })
  guests?: number;
}

@ArgsType()
class openTableNoOrderArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String)
  tableId: string;

  @Field(() => Int, { nullable: true })
  duration?: number;
}

@ArgsType()
class closeTableArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String)
  tableId: string;

  @Field(() => String, { nullable: true })
  status?: OrderHistoryStatus;

  @Field(() => String, { nullable: true })
  comment?: string;
}

@Resolver()
export default class TableResolver {
  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Order)
  async openTable(
    @Args() { orderId, tableId, duration, guests }: openTableArgs,
    @Ctx() { req: { company, employee } }: ContextType
  ): Promise<Order> {
    try {
      const order = await Order.findOne(orderId, { where: { company } });

      if (!order) {
        throw new Error("NO ORDER");
      }

      if (duration) {
        order.duration = duration;
      }
      if (guests) {
        order.guests = guests;
      }
      if (tableId) {
        order.tableId = tableId;
      }

      const table = await Table.findOne(order.tableId, {
        where: { company, isArchived: false },
      });

      if (!table) {
        throw new Error("WRONG TABLE");
      } else if (!table.isActive) {
        throw new Error("TABLE NOT ACTIVE");
      } else if (table.isBusy) {
        throw new Error("TABLE BUSY");
      }

      const timetableService = await TimetableService.build(company!);

      let datetime = moment.utc(order.datetime);
      if (moment.utc() < datetime) {
        datetime = moment.utc(
          roundTime(moment.utc().format("HH:mm"), config.orderDelta),
          "HH:mm"
        );
        order.datetime = datetime.format();
      }

      const timetable = timetableService.getTimetable(datetime, [orderId]);
      const tables = timetableService.getAvailableTables(
        timetable,
        datetime.format("HH:mm"),
        order.duration,
        order.guests
      );

      if (!tables.some((table) => table.split(":")[0] === order.tableId)) {
        throw new Error("BAD ORDER");
      }

      await getManager().transaction(async (t) => {
        order.status = OrderStatus.InProcess;
        order.employee = employee!;
        await t.save(order);

        table.isBusy = true;
        await t.save(t);
      });

      return order;
    } catch (e) {
      console.log(e);
      throw new ApolloError("BAD REQUEST", "400");
    }
  }

  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Order)
  async openTableNoOrder(
    @Args() { tableId, duration }: openTableNoOrderArgs,
    @Ctx() { req: { company, employee } }: ContextType
  ): Promise<Order> {
    try {
      const table = await Table.findOne(tableId, {
        where: { company, isArchived: false },
      });

      if (!table) {
        throw new Error("WRONG TABLE");
      } else if (!table.isActive) {
        throw new Error("TABLE NOT ACTIVE");
      } else if (table.isBusy) {
        throw new Error("TABLE BUSY");
      }

      const orderDuration = duration || company!.defaultDuration;
      const orderDatetime = moment.utc(
        roundTime(moment.utc().format("HH:mm"), config.orderDelta),
        "HH:mm"
      );

      const timetableService = await TimetableService.build(company!);

      const timetable = timetableService.getTimetable(orderDatetime);
      const tables = timetableService.getAvailableTables(
        timetable,
        orderDatetime.format("HH:mm"),
        orderDuration
      );

      if (!tables.some((table) => table.split(":")[0] === tableId)) {
        throw new Error("BAD ORDER");
      }

      const order = new Order();

      order.company = company!;
      order.datetime = orderDatetime.format();
      order.guests = 1;
      order.duration = orderDuration;
      order.employee = employee!;
      order.table = table;
      order.status = OrderStatus.InProcess;

      await getManager().transaction(async (t) => {
        await t.save(order);

        table.isBusy = true;
        await t.save(table);
      });

      return order;
    } catch (e) {
      console.log(e);
      throw new ApolloError("BAD REQUEST", "400");
    }
  }

  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Boolean)
  async closeTable(
    @Args() { tableId, comment, status }: closeTableArgs,
    @Ctx() { req: { company, employee } }: ContextType
  ): Promise<boolean> {
    try {
      const table = await Table.findOne(tableId, {
        where: { company, isArchived: false },
      });

      if (!table) {
        throw new Error("WRONG TABLE");
      } else if (!table.isActive) {
        throw new Error("TABLE NOT ACTIVE");
      } else if (!table.isBusy) {
        throw new Error("TABLE NOT BUSY");
      }

      const order = await Order.findOne({
        where: { table, status: OrderStatus.InProcess },
      });

      if (!order) {
        throw new Error("NO ORDER");
      }

      const orderHistory = new OrderHistory();

      orderHistory.company = company!;
      orderHistory.table = table;
      orderHistory.employee = employee!;
      orderHistory.clientPhoneNumber = order.clientPhoneNumber;
      orderHistory.clientName = order.clientName;
      orderHistory.guests = order.guests;
      orderHistory.datetime = order.datetime;
      orderHistory.duration = order.duration;
      orderHistory.status = status || OrderHistoryStatus.Successed;
      orderHistory.comment = comment;

      await getManager().transaction(async (t) => {
        await t.save(orderHistory);
        await t.remove(order);

        table.isBusy = false;
        await t.save(table);
      });

      return true;
    } catch (e) {
      console.log(e);
      throw new ApolloError("BAD REQUEST", "400");
    }
  }
}
