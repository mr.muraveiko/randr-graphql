import {
  Resolver,
  FieldResolver,
  Root,
  Query,
  Ctx,
  Directive,
} from "type-graphql";

import RUser from "@entity/RUser";
import Employee from "@entity/Employee";

import { ContextType } from "utils";

@Resolver(() => RUser)
export default class RUserResolver {
  @Directive("@auth")
  @Query(() => RUser)
  me(@Ctx() { req: { rUser } }: ContextType): RUser {
    return rUser!;
  }

  @FieldResolver(() => [Employee])
  async employers(@Root() rUser: RUser): Promise<Array<Employee>> {
    const employers = await Employee.find({
      where: { rUser, isArchived: false },
      relations: ["company"],
    });
    return employers;
  }
}
