import {
  Resolver,
  Mutation,
  ArgsType,
  Args,
  Ctx,
  Field,
  Directive,
  Int,
} from "type-graphql";
import { ApolloError } from "apollo-server";
import { Matches } from "class-validator";
import moment from "moment";

import TimetableService from "@services/Timetable";
import Order, { OrderStatus } from "@entity/Order";
import Table from "@entity/Table";

import { ContextType } from "@utils";

@ArgsType()
class openOrderArgs {
  @Field(() => String)
  companyId: string;

  @Field(() => String)
  tableId: string;

  @Field(() => String)
  @Matches(
    /^(\d{4})-(\d{1,2})-(\d{1,2})T([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/
  )
  datetime: string;

  @Field(() => Int, { nullable: true })
  duration?: number;

  @Field(() => Int, { nullable: true })
  guests?: number;

  @Field(() => String, { nullable: true })
  phoneNumber?: string;

  @Field(() => String, { nullable: true })
  clientName?: string;
}

@Resolver()
export default class OrderResolver {
  @Directive("@hasCompanyAccess")
  @Directive("@auth")
  @Mutation(() => Order)
  async openOrder(
    @Args()
    {
      tableId,
      datetime,
      guests,
      duration,
      phoneNumber,
      clientName,
    }: openOrderArgs,
    @Ctx() { req: { company, employee } }: ContextType
  ): Promise<Order> {
    try {
      const table = await Table.findOne(tableId, { where: { company } });

      if (!table) {
        throw new Error("WRONG TABLE");
      } else if (!table.isActive) {
        throw new Error("TABLE NOT ACTIVE");
      } else if (table.isBusy) {
        throw new Error("TABLE BUSY");
      }

      const orderDatetime = moment.utc(datetime);
      const orderDuration = duration || company!.defaultDuration;
      const orderGuests = guests || 1;

      const timetableService = await TimetableService.build(company!);

      const timetable = timetableService.getTimetable(orderDatetime);
      const tables = timetableService.getAvailableTables(
        timetable,
        orderDatetime.format("HH:mm"),
        orderDuration,
        orderGuests
      );

      if (!tables.some((table) => table.split(":")[0] === tableId)) {
        throw new Error("BAD ORDER");
      }

      const order = new Order();

      order.company = company!;
      order.datetime = orderDatetime.format();
      order.guests = orderGuests;
      order.duration = orderDuration;
      order.employee = employee!;
      order.table = table;
      order.status = OrderStatus.Confirmed;
      order.clientPhoneNumber = phoneNumber;
      order.clientName = clientName;

      await order.save();

      return order;
    } catch (e) {
      console.log(e);
      throw new ApolloError("BAD ORDER REQUEST", "400");
    }
  }
}
