import { ApolloServer } from "apollo-server-express";
import { buildSchemaSync } from "type-graphql";
import { SchemaDirectiveVisitor } from "graphql-tools";
import {
  apolloContext,
  AuthDirective,
  HasCompanyAccessDirective,
} from "@utils";

const schema = buildSchemaSync({
  resolvers: [__dirname + "/resolvers/**/*.resolver.js"],
});

SchemaDirectiveVisitor.visitSchemaDirectives(schema, {
  auth: AuthDirective,
  hasCompanyAccess: HasCompanyAccessDirective,
});

export const apolloServer = new ApolloServer({
  schema,
  playground: true,
  debug: true,
  context: apolloContext(),
});
